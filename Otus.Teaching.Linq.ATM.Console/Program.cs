using System;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Console
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            System.Console.WriteLine("ATM");
            while (true)
            {
                System.Console.WriteLine("Введите '1' для вывода информации о аккаунте по логину и паролю");
                System.Console.WriteLine("Введите '2' для вывода информации о всех счетах пользователя");
                System.Console.WriteLine("Введите '3' для вывода информации о всех счетах с историей пользователя");
                System.Console.WriteLine("Введите '4' для вывода информации о аккаунте по логину и паролю");
                System.Console.WriteLine("Введите '5' для вывода информации о всех пользователях у которых на счёте сумма больше N");
                System.Console.WriteLine("Введите 'exit' для завершенияч работы");
                string key = System.Console.ReadLine();
                System.Console.WriteLine();
                var atmManager = CreateATMManager();
                switch (key)
                {                       
                case "1":
                        System.Console.WriteLine("1 Вывод информации о заданном аккаунте по логину и паролю");
                        Task1(atmManager);
                        break;
                 case "2":
                        System.Console.WriteLine("2 Вывод данных о всех счетах заданного пользователя");
                        Task2(atmManager);
                        break;
                case "3":
                        System.Console.WriteLine("3 Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту");
                        Task3(atmManager);
                        break;
                case "4":
                        System.Console.WriteLine("4 Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта");
                        Task4(atmManager);
                        break;
                case "5":
                        System.Console.WriteLine("5 Вывод данных о всех пользователях у которых на счёте сумма больше N. Введите сумму:");
                        try
                        {
                            System.Console.WriteLine(atmManager.GetUsersWhoHaveBalanceMoreThen(Convert.ToDecimal(System.Console.ReadLine())));
                        }
                        catch
                        {
                            System.Console.WriteLine("Некорректно введена сумма!");
                        }
                        break;
                    default:
                        System.Console.WriteLine("Неверный формат команды...");
                        break;
                    case "exit":
                        System.Console.WriteLine("Работа завершена");
                        return;
                }
                }
            }

        // 1 Вывод информации о заданном аккаунте по логину и паролю;
        private static void Task1(ATMManager atmManager)
        {
            System.Console.WriteLine(Divider);
            System.Console.WriteLine("Введите логин:");
            string login = System.Console.ReadLine();
            System.Console.WriteLine("Введите пароль:");
            string pwd = System.Console.ReadLine();
            WriteUserInfo(atmManager, login, pwd);
        }

        // 2 Вывод данных о всех счетах заданного пользователя;
        private static void Task2(ATMManager atmManager)
        {
            System.Console.WriteLine(Divider);
            System.Console.WriteLine("Введите логин:");
            string login = System.Console.ReadLine();
            System.Console.WriteLine("Введите пароль:");
            string pwd = System.Console.ReadLine();
            var usr = atmManager.GetUsrInfo(login, pwd);
            System.Console.WriteLine($"Информация по счетам пользователя:{ login}");
            WriteUserAccounts(atmManager, usr);
        }

        // 3 Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту;
        private static void Task3(ATMManager atmManager)
        {
            System.Console.WriteLine(Divider);
            System.Console.WriteLine("Введите логин:");
            string login = System.Console.ReadLine();
            System.Console.WriteLine("Введите пароль:");
            string pwd = System.Console.ReadLine();
            var usr = atmManager.GetUsrInfo(login, pwd);
            System.Console.WriteLine($"Информация по счетам пользователя с историей:{ login}");
            atmManager.GetUsrAccWithHis(usr);
        }

        // 4 Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта;
        private static void Task4(ATMManager atmManager)
        {
            System.Console.WriteLine(Divider);
            System.Console.WriteLine("4 задание: Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта");

            WriteInputCashOperations(atmManager);
        }

        private static void WriteUserInfo(ATMManager atmManager, string login, string password)
        {
            var user = atmManager.GetUsrInfo(login, password);
            System.Console.WriteLine(user == null
                ? $"    Пользователя с логином '{login}' и паролем '{password}' не существует"
                : $"    Пользователя с логином '{login}' и паролем '{password}' зовут {user.FirstName} {user.SurName}");
        }

        private static void WriteUserAccounts(ATMManager atmManager, User user)
        {
            var accounts = atmManager.GetUsrAcc(user);
            System.Console.WriteLine(accounts.Any()
                ? $"Количество счетов у пользователя {user.FirstName} {user.SurName}: {accounts.Count}"
                : $"У пользователя {user.FirstName} {user.SurName} нет счетов");

            foreach (var account in accounts)
            {
                System.Console.WriteLine($"   Счёт с балансом ${account.CashAll} открытый {account.OpeningDate:d}");
            }

            System.Console.WriteLine();
        }

        private static void WriteInputCashOperations(ATMManager atmManager)
        {
            var inputCashOperations = atmManager.GetInputCashOperations();
            System.Console.WriteLine(inputCashOperations.Any()
                ? $"Количество транзакций с пополнениями: {inputCashOperations.Count}"
                : "Нет транзакций с пополнениями");

            foreach (var (operation, account, user) in inputCashOperations)
            {
                System.Console.WriteLine($"    Пополнение {operation.OperationDate:d} на сумму {operation.CashSum} " +
                                         $"по счёту, открытому {account.OpeningDate:d} у пользователя {user.FirstName} {user.SurName}");
            }

            System.Console.WriteLine();
        }

        private static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }
        private const string Divider = "\n----------------------------------------------------------------------------------------------------------------------";
    }
}
