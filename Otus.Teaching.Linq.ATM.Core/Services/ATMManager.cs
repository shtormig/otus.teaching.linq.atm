
using System.Collections.Generic;
using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Linq;
using System;
using System.Text.Json;


namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }
        
        public IEnumerable<User> Users { get; private set; }
        
        public IEnumerable<OperationsHistory> History { get; private set; }
        
        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        //TODO: Добавить методы получения данных для банкомата
       
        // Получение информации о пользователе по логину и паролю
        public User GetUsrInfo(string login, string pass)
        {
            return Users.FirstOrDefault(u =>
                u.Login.Equals(login, StringComparison.Ordinal) &&
                u.Password.Equals(pass, StringComparison.Ordinal));
        }
        
        // Получение информации о счетах по пользователю
        public List<Account> GetUsrAcc(User user)
        {
            return Accounts.Where(a => a.UserId == user.Id).ToList();
        }

        // Получение информации о счетах с историей
        public Dictionary<Account, List<OperationsHistory>> GetUsrAccWithHis(User user)
        {
            return Accounts
                .Where(account => account.UserId == user.Id)
                .GroupJoin(History,
                    account => account.Id,
                    history => history.AccountId,
                    (account, history) => new
                    {
                        Account = account,
                        Operations = history.Select(o => o).ToList()
                    })
                .ToDictionary(account => account.Account, account => account.Operations);
        }


        public List<Tuple<OperationsHistory, Account, User>> GetInputCashOperations()
        {
            return History
                .Where(operation => operation.OperationType == OperationType.InputCash)
                .Join(Accounts,
                    history => history.AccountId,
                    account => account.Id,
                    (history, account) => new { History = history, Account = account })
                .Join(Users,
                    accountWithHistory => accountWithHistory.Account.UserId,
                    user => user.Id,
            (accountWithHistory, user) => new { AccountWithHistory = accountWithHistory, User = user })
                .Select(i => new Tuple<OperationsHistory, Account, User>(i.AccountWithHistory.History, i.AccountWithHistory.Account, i.User)).ToList();
        }

        public string GetUsersWhoHaveBalanceMoreThen(decimal minBalance)
        {
            // 5 задание: Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой)
            var infoUserWithMoney = (from
                                      account in Accounts.Where(x => x.CashAll > minBalance)
                                     group account by account.UserId into g
                                     join user in Users on g.Key equals user.Id
                                     select new { user.Login, user.FirstName, user.SurName, user.PassportSeriesAndNumber, user.Phone, user.RegistrationDate });

            return JsonSerializer.Serialize(infoUserWithMoney, new JsonSerializerOptions() { WriteIndented = true });

        }

    }
}
